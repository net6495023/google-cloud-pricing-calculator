﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Configuration
{
    public class AppConfigReader : IConfig
    {
        public string GetURL()
        {
            return ConfigurationHelper.GetConfiguration().GetValue<string>(AppConfigKeys.URL);
        }
    }
}
