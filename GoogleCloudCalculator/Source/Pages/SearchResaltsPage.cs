﻿using GoogleCloudCalculator.Source.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Pages
{
    public class SearchResaltsPage : Driver
    {
        [FindsBy(How = How.XPath, Using = "//a/b[text()='Google Cloud Pricing Calculator']/..")]
        private readonly IWebElement _searchResultLink;

        public SearchResaltsPage()
        {
            PageFactory.InitElements(_driver, this);
        }

        public void OpenLink()
        {
            _searchResultLink.Click();
        }
    }
}
