﻿using GoogleCloudCalculator.Source.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Pages
{
    public class InboxPage : Driver
    {
        [FindsBy(How = How.XPath, Using = "(//table/tbody/tr/td/h3)[2]")]
        private readonly IWebElement _mail;

        [FindsBy(How = How.XPath, Using = "//button[@id='refresh']")]
        private readonly IWebElement _refresh;

        public InboxPage()
        {
            PageFactory.InitElements(_driver2, this);
        }

        public string GetPrice()
        {
            WebDriverWait wait = new(_driver2, TimeSpan.FromSeconds(500));
            wait.Until(d => _refresh.Displayed);
            _refresh.Click();
            _driver2.SwitchTo().Frame("ifmail");
            wait.Until(d => _mail.Displayed);
            return _mail.Text;       
        }
    }
}
