﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebDriverManager.DriverConfigs.Impl;
using WebDriverManager;
using NUnit.Framework;
using System.Configuration;
using OpenQA.Selenium.Edge;
using GoogleCloudCalculator.Source.Configuration;
using Microsoft.Extensions.Configuration;
using NUnit.Framework.Interfaces;
using WDSE;
using WDSE.Decorators;
using WDSE.ScreenshotMaker;

namespace GoogleCloudCalculator.Source.Drivers
{
    public class Driver
    {
        public static IWebDriver _driver;
        public static IWebDriver _driver2;

        [SetUp]
        public void Setup()
        {
            
            switch (Environment.GetEnvironmentVariable("Browser"))
            {
                case "edge":
                    new DriverManager().SetUpDriver(new EdgeConfig());
                    _driver = new EdgeDriver();
                    break;
                default:
                    new DriverManager().SetUpDriver(new ChromeConfig());
                    _driver = new ChromeDriver();
                    break;
            }

            IConfig config = new AppConfigReader();

            _driver.Navigate().GoToUrl(config.GetURL());
            _driver.Manage().Window.Maximize();
        }

        [TearDown]
        public void Quit()
        {
            if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
            {
                Screenshot ss = ((ITakesScreenshot)_driver).GetScreenshot();
                ss.SaveAsFile("Screenshot_" + DateTime.Now.ToString("MM.dd.yyyy HH.mm") + ".png", ScreenshotImageFormat.Png);
            }

            _driver.Quit();

            if (_driver2 is not null)
                _driver2.Quit();
        }
    }
}
