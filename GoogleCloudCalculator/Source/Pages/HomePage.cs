﻿using GoogleCloudCalculator.Source.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Pages
{
    public class HomePage : Driver
    {
        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Search']/..")]
        private readonly IWebElement _searchLink;

        [FindsBy(How = How.XPath, Using = "//input[@aria-label='Search']")]
        private readonly IWebElement _searchTbox;

        public HomePage ()
        {
            PageFactory.InitElements(_driver, this);
        }

        public void Search (string text)
        {
            _searchLink.Click();
            _searchTbox.SendKeys(text);
            new Actions(_driver).SendKeys(Keys.Enter).Perform();
        }
    }
}
