﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using GoogleCloudCalculator.Source.Drivers;
using GoogleCloudCalculator.Source.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace GoogleCloudCalculator.Tests
{
    public class HomePageTests : Driver
    {
        [Test]
        [Category("LongRunning")]
        public void GoogleCloudPricingCalculator()
        {
            SearchForCloudCalculator();
            OpenLink();

            CalculatorPage cp = new();
            cp.CalculatePrice();
            string expectedResult = cp.GetTotalPrice();
            cp.EmailEstimate();

            new DriverManager().SetUpDriver(new ChromeConfig());
            _driver2 = new ChromeDriver();
            _driver2.Navigate().GoToUrl("https://yopmail.com/en/email-generator");
            _driver2.Manage().Window.Maximize();

            EmailPage ep = new();
            ep.GenerateAndCopyEmail();

            cp.SendEmail();
            string actualResult = ep.CheckEmail();
            Assert.That(actualResult, Is.EqualTo(expectedResult));
        }

        private static void SearchForCloudCalculator()
        {
            HomePage hp = new();
            hp.Search("Google Cloud Pricing Calculator");
            Assert.That(_driver.Title, Does.Contain("Google Cloud Pricing Calculator"));
        }

        private static void OpenLink()
        {
            SearchResaltsPage srp = new();
            srp.OpenLink();
            Assert.That(_driver.Title, Does.Contain("Google Cloud Pricing Calculator"));
        }
    }
}
