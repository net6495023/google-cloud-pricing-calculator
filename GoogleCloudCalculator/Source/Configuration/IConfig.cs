﻿using OpenQA.Selenium.DevTools.V115.Browser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Configuration
{
    public interface IConfig
    {
        string GetURL();
    }
}
