﻿using GoogleCloudCalculator.Source.Drivers;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Pages
{
    public class EmailPage : Driver
    {
        readonly WebDriverWait wait = new(_driver2, TimeSpan.FromSeconds(10));

        [FindsBy(How = How.Id, Using = "necesary")]
        private readonly IWebElement _cookies;

        [FindsBy(How = How.XPath, Using = "//button/span[text()='New']/..")]
        private readonly IWebElement _generateEmail;

        [FindsBy(How = How.XPath, Using = "//button/span/span[text()='Copy']/../..")]
        private readonly IWebElement _copyAddress;

        [FindsBy(How = How.XPath, Using = "//button/span[text()='Check Inbox']/..")]
        private readonly IWebElement _checkInbox;

        public EmailPage()
        {
            PageFactory.InitElements(_driver2, this);
        }

        public void GenerateAndCopyEmail()
        {
            if (_driver2.FindElements(By.XPath("//div/h2[text()='Privacy Settings']")).ToList().Count > 0)
            {
                wait.Until(d => _cookies.Displayed);
                _cookies.Click();
            }

            _generateEmail.Click();

            if (_driver2.FindElements(By.XPath("//div/h2[text()='Privacy Settings']")).ToList().Count > 0)
            {
                wait.Until(d => _cookies.Displayed);
                _cookies.Click();
            }

            _copyAddress.Click();
        }

        public void CheckInbox()
        {
            _checkInbox.Click();
        }

        public string CheckEmail()
        {
            _checkInbox.Click();
            InboxPage ip = new();
            return ip.GetPrice();
        }
    }
}
