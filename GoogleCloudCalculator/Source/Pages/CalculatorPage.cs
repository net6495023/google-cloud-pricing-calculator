﻿using GoogleCloudCalculator.Source.Drivers;
using GoogleCloudCalculator.Source.Model;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Pages
{
    public class CalculatorPage : Driver
    {
        readonly WebDriverWait wait = new(_driver, TimeSpan.FromSeconds(10));

        [FindsBy(How = How.XPath, Using = "(//div[@title='Compute Engine'])[1]/..")]
        private readonly IWebElement _section;

        [FindsBy(How = How.XPath, Using = "//input[@name='quantity']")]
        private readonly IWebElement _numOfInstances;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@name='series']")]
        private readonly IWebElement _series;

        [FindsBy(How = How.XPath, Using = "//md-select-menu/md-content/md-option[@value='n1']")]
        private readonly IWebElement _seriesSelection;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@placeholder='Instance type']")]
        private readonly IWebElement _machineType;

        [FindsBy(How = How.XPath, Using = "//md-select-menu/md-content/md-optgroup/md-option[@value='CP-COMPUTEENGINE-VMIMAGE-N1-STANDARD-8']")]
        private readonly IWebElement _machineTypeSelection;

        [FindsBy(How = How.XPath, Using = "//md-checkbox[@aria-label='Add GPUs']")]
        private readonly IWebElement _addGPUs;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@placeholder='GPU type']")]
        private readonly IWebElement _GPUType;

        [FindsBy(How = How.XPath, Using = "//md-select-menu/md-content/md-option[@value='NVIDIA_TESLA_V100']")]
        private readonly IWebElement _GPUTypeSelection;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@placeholder='Number of GPUs']")]
        private readonly IWebElement _numGPU;

        [FindsBy(How = How.XPath, Using = "//md-select-menu/md-content/md-option[@ng-repeat='item in listingCtrl.supportedGpuNumbers[listingCtrl.computeServer.gpuType]' and @value='1']")]
        private readonly IWebElement _numGPUSelection;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@placeholder='Local SSD']")]
        private readonly IWebElement _localSSD;

        [FindsBy(How = How.XPath, Using = "//md-select-menu/md-content/md-option[@ng-repeat='item in listingCtrl.dynamicSsd.computeServer' and @value='2']")]
        private readonly IWebElement _localSSDSelection;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@placeholder='Datacenter location']")]
        private readonly IWebElement _datacenterLocation;

        [FindsBy(How = How.XPath, Using = "(//md-select-menu/md-content/md-optgroup/md-option/div[contains(text(),'Frankfurt (europe-west3)')]/..)[2]")]
        private readonly IWebElement _datacenterLocationSelection;

        [FindsBy(How = How.XPath, Using = "//md-input-container/md-select[@placeholder='Committed usage']")]
        private readonly IWebElement _usage;

        [FindsBy(How = How.XPath, Using = "(//md-select-menu/md-content/md-option[@value='1']/div[text()='1 Year']/..)[2]")]
        private readonly IWebElement _usageSelection;

        [FindsBy(How = How.XPath, Using = "//md-card/md-card-content/div/div[1]/form/div/button[contains(text(),'Add to Estimate')]")]
        private readonly IWebElement _submit;

        [FindsBy(How = How.XPath, Using = "//div[@type='cookie-notification']/div/button")]
        private readonly IWebElement _cookiesBtn;

        [FindsBy(How = How.XPath, Using = "//button[@title='Email Estimate']")]
        private readonly IWebElement _emailEstimateBtn;

        [FindsBy(How = How.XPath, Using = "(//form[@name='emailForm']/md-content/div/md-input-container/input)[3]")]
        private readonly IWebElement _email;

        [FindsBy(How = How.XPath, Using = "(//form[@name='emailForm']/md-dialog-actions/button)[2]")]
        private readonly IWebElement _sendEmail;

        [FindsBy(How = How.XPath, Using = "//b[contains(text(), 'Total Estimated Cost')]")]
        private readonly IWebElement _totalPrice;

        public CalculatorPage()
        {
            PageFactory.InitElements(_driver, this);
        }

        public void CalculatePrice ()
        {
            if (_driver.FindElements(By.XPath("//div[@type='cookie-notification']")).ToList().Count > 0)
            {
                wait.Until(d => _cookiesBtn.Displayed);
                _cookiesBtn.Click();
            }

            _driver.SwitchTo().Frame(0);
            _driver.SwitchTo().Frame("myFrame");

            Product p = new(_section, _numOfInstances, _series, _seriesSelection, _machineType, _machineTypeSelection, _addGPUs, _GPUType, _GPUTypeSelection,
                _numGPU, _numGPUSelection, _localSSD, _localSSDSelection, _datacenterLocation, _datacenterLocationSelection, _usage, _usageSelection);

            wait.Until(d => p.section.Displayed);
            p.section.Click();
            p.numOfInstances.SendKeys("4");
            p.series.Click();
            wait.Until(d => p.seriesSelection.Displayed);
            p.seriesSelection.Click();
            p.machineType.Click();
            wait.Until(d => p.machineTypeSelection.Displayed);
            p.machineTypeSelection.Click();
            p.addGPUs.Click();
            p.GPUType.Click();
            wait.Until(d => p.GPUTypeSelection.Displayed);
            p.GPUTypeSelection.Click();
            p.numGPU.Click();
            wait.Until(d => p.numGPUSelection.Displayed);
            p.numGPUSelection.Click();
            p.localSSD.Click();
            wait.Until(d => p.localSSDSelection.Displayed);
            p.localSSDSelection.Click();
            p.datacenterLocation.Click();
            wait.Until(d => p.datacenterLocationSelection.Displayed);
            p.datacenterLocationSelection.Click();
            p.usage.Click();
            wait.Until(d => p.usageSelection.Displayed);
            p.usageSelection.Click();
            _submit.Click();
        }

        public void EmailEstimate()
        {
            _emailEstimateBtn.Click();
        }

        public string GetTotalPrice()
        {
            wait.Until(d => _totalPrice.Displayed);
            return _totalPrice.Text.Replace("Total Estimated Cost: ","").Replace(" per 1 month","");
        }

        public void SendEmail()
        {
            wait.Until(d => _email.Displayed);
            _email.Click();
            _email.SendKeys(Keys.Control + 'v');
            _sendEmail.Click();
        }
    }
}
