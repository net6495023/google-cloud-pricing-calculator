﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace GoogleCloudCalculator.Source.Model
{
    public class Product
    {
        public readonly IWebElement section;
        public readonly IWebElement numOfInstances;
        public readonly IWebElement series;
        public readonly IWebElement seriesSelection;
        public readonly IWebElement machineType;
        public readonly IWebElement machineTypeSelection;
        public readonly IWebElement addGPUs;
        public readonly IWebElement GPUType;
        public readonly IWebElement GPUTypeSelection;
        public readonly IWebElement numGPU;
        public readonly IWebElement numGPUSelection;
        public readonly IWebElement localSSD;
        public readonly IWebElement localSSDSelection;
        public readonly IWebElement datacenterLocation;
        public readonly IWebElement datacenterLocationSelection;
        public readonly IWebElement usage;
        public readonly IWebElement usageSelection;

        public Product(params IWebElement[] elements)
        {
            section = elements[0];
            numOfInstances = elements[1];
            series = elements[2];
            seriesSelection = elements[3];
            machineType = elements[4];
            machineTypeSelection = elements[5];
            addGPUs = elements[6];
            GPUType = elements[7];
            GPUTypeSelection = elements[8];
            numGPU = elements[9];
            numGPUSelection = elements[10];
            localSSD = elements[11];
            localSSDSelection = elements[12];
            datacenterLocation = elements[13];
            datacenterLocationSelection = elements[14];
            usage = elements[15];
            usageSelection = elements[16];
        }
    }
}
