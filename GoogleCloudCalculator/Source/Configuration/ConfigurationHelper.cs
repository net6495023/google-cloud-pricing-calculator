﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoogleCloudCalculator.Source.Configuration
{
    public class ConfigurationHelper
    {
        public static IConfigurationSection GetConfiguration()
        {
            // local.json file is used to set credentials locally, this file is added to .gitignore
            var configRoot = new ConfigurationBuilder()
                .SetBasePath(TestContext.CurrentContext.TestDirectory)
                .AddJsonFile(Environment.GetEnvironmentVariable("Env"), optional: true)
                //.AddJsonFile("configsetting.local.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();
            
            var configSec = configRoot.GetSection("EnvSettings");

            return configSec;
        }
    }
}
